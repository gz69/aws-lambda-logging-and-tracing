# Mini-project Week 6

## Target
The goal of this mini-project is to enhance a Rust Lambda function by adding logging, integrating AWS X-Ray tracing, and connecting logs/traces to CloudWatch.

## Steps

1. **Prepare Rust Lambda Function:**
   - Ensure you have a Rust Lambda Function ready.

2. **Modify Cargo.toml Dependencies:**
   - Update dependencies in `Cargo.toml`:
     ```toml
     tracing = "0.1.37"
     tracing-subscriber = "0.2.0"
     ```

3. **Add Logging Initialization:**
   - Add the following code at the beginning of the `main` function in `main.rs`:
     ```rust
     // Initialize logging
     use tracing_subscriber::FmtSubscriber;
     use tracing::Level;
     use tracing::{info, warn};

     let subscriber = FmtSubscriber::builder()
         .with_max_level(Level::INFO)
         .finish();

     tracing::subscriber::set_global_default(subscriber).expect("Unable to set global default");

     info!("Starting the redactr service");
     warn!("This is a warning");
     ```

4. **Deploy Lambda Function to AWS:**
   - Build and deploy the Lambda function to AWS with an IAM role containing permissions:
     - AWSLambda_FullAccess
     - AWSLambdaBasicExecutionRole
     - IAMFullAccess
     - AWSXRayDaemonWriteAccess

5. **Enable AWS X-Ray Tracing:**
   - In AWS Lambda console, navigate to Configuration -> Monitoring and operations tools, and enable AWS X-Ray tracing.

6. **Test Logging and X-Ray Tracing Locally:**
   - Run `cargo lambda watch` to test logging and X-Ray tracing locally.

7. **Test Remotely:**
   - For remote testing, run:
     ```
     cargo lambda invoke --remote mini-project6 --data-ascii "{\"x\":-1, \"y\":21 }"
     ```

8. **Check Logs and Traces:**
   - Check for logging and traces in the Monitor section of the AWSLambda.