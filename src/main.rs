use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};
use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;

#[derive(Deserialize)]
struct QueryParams {
    x: i32,
    y: i32,
}

#[derive(Serialize)]
struct Response {
    total: i32,
}

async fn add(query: web::Query<QueryParams>) -> impl Responder {
    info!("Starting the redactr service");
    let total = query.x + query.y;
    let resp = Response { total };
    HttpResponse::Ok().json(resp)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {

    // Initialze logging
    let subscriber = FmtSubscriber::builder().with_max_level(Level::INFO).finish();

    tracing::subscriber::set_global_default(subscriber).expect("Unable to set glocal default");


    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(add))
    })
    .bind("localhost:9000")?
    .run()
    .await
}
